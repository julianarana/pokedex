import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { PokedexModule } from './modules/pokedex/pokedex.module';
import { MenuComponent } from './shared/menu/menu.component';
import { UsersDataService } from './modules/authentication/shared/services/users-data.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
  ],
  imports: [
    AppRoutingModule,
    AuthenticationModule,
    BrowserAnimationsModule,
    BrowserModule,
    PokedexModule,
  ],
  providers: [UsersDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
