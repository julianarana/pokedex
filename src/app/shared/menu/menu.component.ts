import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { UsersDataService } from 'src/app/modules/authentication/shared/services/users-data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements DoCheck, OnInit {
  protected url: string;

  constructor(private router: Router, private usersService: UsersDataService) { }

  ngOnInit() {
  }

  ngDoCheck() {
    this.url = this.router.url;
  }

  handleLogout() {
    this.usersService.doLogout();
    this.router.navigate(['/login']);
  }

}
