import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validatePassword } from '../../shared/utils/Validators';
import { UsersDataService } from '../../shared/services/users-data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  protected registerForm: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(/\S+@\S+\.\S+/)
    ]),
    passwords: new FormGroup({
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(?=.*[a-z].*[a-z])(?=.*[A-Z].*[A-Z])(?=.*\d)(?=.*\W.*\W)[a-zA-Z0-9\S]{8,}$/),
        Validators.minLength(8),
      ]),
      passwordConfirm: new FormControl('', [
        Validators.required,
      ]),
    }, [validatePassword])
  });

  constructor(private router: Router, private userService: UsersDataService) { }

  ngOnInit() {
  }

  register() {
    if (this.registerForm.valid) {
      this.userService.addUser({
        name: this.registerForm.value.name,
        email: this.registerForm.value.email,
        password: this.registerForm.value.passwords.password,
      });
      this.router.navigate(['/login']);
    }
  }

  login() {
    this.router.navigate(['/login']);
  }

}
