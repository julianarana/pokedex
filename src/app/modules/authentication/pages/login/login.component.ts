import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersDataService } from '../../shared/services/users-data.service';
import { User } from '../../shared/types/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  protected loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(private router: Router, private userService: UsersDataService) { }

  ngOnInit() {
  }

  enter() {
    if (this.loginForm.valid) {
      const user: User = this.userService.doLogin(
        this.loginForm.value.email, this.loginForm.value.password);
      if (!!user) {
        this.router.navigate(['/pokedex']);
      } else {
        this.loginForm.setErrors({ notValid: true });
      }
    }
  }

  register() {
    this.router.navigate(['/register']);
  }

}
