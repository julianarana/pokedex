import { Injectable } from '@angular/core';
import { User } from '../types/User';
import { users } from '../mocks/users';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {

  private registeredUsers: User[] = [...users];

  private user: User;

  constructor() { }

  public addUser(user: User): void {
    this.registeredUsers.push(user);
  }

  private findUser(email: string): User {
    return this.registeredUsers.find((user: User) => user.email === email);
  }

  public doLogin(email: string, password: string): User {
    const user: User = this.findUser(email);
    if (!!user && user.password === password) {
      this.user = user;
    }
    return this.user;
  }

  public getUser(): User {
    return this.user;
  }

  public doLogout(): void {
    this.user = null;
  }

  public isLogged(): boolean {
    return !!this.user;
  }
}
