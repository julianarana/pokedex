import {FormGroup } from '@angular/forms';

export function validatePassword(passwords: FormGroup) {
  const pass: string = passwords.get('password').value;
  const passwordConfirm: string = passwords.get('passwordConfirm').value;

  return pass === passwordConfirm ? null : { different: true };
}
