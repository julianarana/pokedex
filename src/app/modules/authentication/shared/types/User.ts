export interface User {
  name: string;
  email: string;
  password: string; // For the sake of the application this will be stored as a plain text
}
