import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UsersDataService } from './shared/services/users-data.service';

@NgModule({
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    AuthenticationRoutingModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  providers: [
    UsersDataService
  ]
})
export class AuthenticationModule { }
