import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './pages/list/list.component';
import { DetailsComponent } from './pages/details/details.component';
import { PokedexRoutingModule } from './pokedex-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ListItemComponent } from './shared/components/list-item/list-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ArrayToCommaStringPipe } from './shared/pipes/array-to-comma-string.pipe';

@NgModule({
  declarations: [
    DetailsComponent,
    ListComponent,
    ListItemComponent,
    ArrayToCommaStringPipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    PokedexRoutingModule,
  ],
  exports: [

  ]
})
export class PokedexModule { }
