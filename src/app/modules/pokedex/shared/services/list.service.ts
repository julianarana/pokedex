import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Pokemon, Type } from '../types/Pokemon';
import { catchError, concatAll, reduce, flatMap, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { PokemonResource, ApiResource } from '../types/Resources';
import { BehaviorSubject } from 'rxjs';

const URL_PREFIX = 'https://pokeapi.co/api/v2/';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private selected: number;
  private _pokemons: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);
  private pokemons: Observable<Pokemon[]> = this._pokemons.asObservable();

  constructor(private http: HttpClient) {
    this.loadPokemons();
  }

  public loadPokemons(): Observable<Pokemon[]> {
    const listUrl = `${URL_PREFIX}pokemon?limit=50&offset=0`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    this.http.get(listUrl, { headers })
      .pipe(
        flatMap((result: ApiResource): Observable<Pokemon>[] => {
          return result.results.map((resource: PokemonResource): Observable<Pokemon> => {
            return this.http.get(resource.url, { headers }) as Observable<Pokemon>
          });
        }),
        concatAll(),
        reduce((prev: Pokemon[], element: Pokemon): Pokemon[] => {
          prev.push(element);
          return prev;
        }, []),
        catchError((error: string): Observable<never> => throwError(error))
      ).subscribe((pokemons: Pokemon[]) => this._pokemons.next(pokemons));

    return this.pokemons;
  }

  public select(position: number) {
    this.selected = position;
  }

  public getSelected(): Pokemon {
    const currentPokemons: Pokemon[] = this._pokemons.value;
    if (this.selected >= 0 && this.selected < currentPokemons.length) {
      return currentPokemons[this.selected];
    }
    return null;
  }

  public getPokemons(): Observable<Pokemon[]> {
    return this.pokemons;
  }

  public getPokemonsByKeyword(keyword: string): Pokemon[] {
    const currentPokemons: Pokemon[] = this._pokemons.value;
    return currentPokemons.filter((pokemon: Pokemon) => {
      const regex: RegExp = new RegExp(keyword, 'g');
      return pokemon.name && pokemon.name.match(regex) || pokemon.types.some((value: Type) => {
        return value.type.name && value.type.name.match(regex);
      });
    });
  }
}
