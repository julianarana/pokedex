export interface Pokemon {
  // abilities: (2) [{…}, {…}]
  base_experience: number;
  // forms: [{…}]
  // game_indices: (20) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
  height: number;
  //held_items: []
  id: number;
  is_default: boolean;
  location_area_encounters: string;
  moves: Move[],//(108) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, …]
  name: string;
  order: number;
  //species: {name: "charizard", url: "https://pokeapi.co/api/v2/pokemon-species/6/"}
  sprites: Sprites;
  //stats: (6) [{…}, {…}, {…}, {…}, {…}, {…}]
  types: Type[];
  weight: number;
}

export interface Move {
  move: { name: string },
  
}

export interface Ability {

}

export interface Form {

}

export interface Sprites {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
}

export interface Type {
  slot: number;
  type: { name: string, url: string };
}