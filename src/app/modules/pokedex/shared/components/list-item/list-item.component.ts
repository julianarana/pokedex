import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Pokemon } from '../../types/Pokemon';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input()
  public item: Pokemon;

  @Output()
  public selected: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  select() {
    this.selected.emit();
  }

}
