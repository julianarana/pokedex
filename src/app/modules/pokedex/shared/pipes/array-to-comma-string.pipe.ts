import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayToCommaString'
})
export class ArrayToCommaStringPipe implements PipeTransform {

  transform(value: string[], ...args: any[]): string {
    return value.join(', ');
  }

}
