import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListService } from '../../shared/services/list.service';
import { Pokemon } from '../../shared/types/Pokemon';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnDestroy, OnInit {

  protected items: Pokemon[] = [];
  private itemsSubscription: Subscription;
  private valueChangesSubscription: Subscription;
  protected searchForm: FormGroup = new FormGroup({
    search: new FormControl(''),
  });

  constructor(private pokemonListSvc: ListService, private router: Router) { }

  ngOnInit() {
    this.itemsSubscription = this.pokemonListSvc.getPokemons()
      .subscribe((pokemons: Pokemon[]) => {
        this.items = pokemons;
      });
    this.valueChangesSubscription = this.searchForm.get('search').valueChanges
      .pipe(debounceTime(400)).subscribe((value): void => {
        this.items = this.pokemonListSvc.getPokemonsByKeyword(value);
      });
  }

  ngOnDestroy() {
    this.itemsSubscription.unsubscribe();
    this.valueChangesSubscription.unsubscribe();
  }

  selectItem(position: number) {
    this.pokemonListSvc.select(position);
    this.router.navigate(['/details']);
  }


}
