import { Component, OnInit } from '@angular/core';
import { ListService } from '../../shared/services/list.service';
import { Router } from '@angular/router';
import { Pokemon, Move, Type } from '../../shared/types/Pokemon';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {


  protected pokemon: Pokemon;
  protected moves: string[] = [];
  protected types: string[] = [];

  constructor(private pokemonListSvc: ListService, private router: Router) { }

  ngOnInit() {
    this.pokemon = this.pokemonListSvc.getSelected();
    if (!this.pokemon) {
      return this.router.navigate(['/pokedex']);
    }
    this.moves = this.pokemon.moves.map((move: Move): string => {
      return move.move.name;
    });
    this.types = this.pokemon.types.map((type: Type): string => {
      return type.type.name;
    });
  }

}
