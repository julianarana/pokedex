import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './pages/list/list.component';
import { DetailsComponent } from './pages/details/details.component';
import { AuthGuardServiceService } from '../authentication/shared/services/auth-guard-service.service';

const pokedexRoutes: Routes = [
  { path: 'pokedex', component: ListComponent, canActivate: [AuthGuardServiceService]  },
  { path: 'details', component: DetailsComponent, canActivate:[AuthGuardServiceService] },
];

@NgModule({
  imports: [
    RouterModule.forChild(pokedexRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PokedexRoutingModule { }
