# PokedexApp

This demo allow the user to create locally some pokemons with an image, a name and a type. 

## External libraries Used
No external libraries other than Angular, RxJS and Material were used.

Sass preprocessor was used in order to compile the classes used on the code. The layout was built with Raw Sass code.


## Code Organization
Application was divided into two modules:

The module `authentication` contains all the code related to the login and the registration, including the logic behind maintaining a session.

The module `pokedex` contains the code that displays the pokemons as a list and as with its details

If further funcionalities are meant to be built, those can be written in different modules.

### Folders inside Modules

On each module thenre are several folders:
Contains the different pages of the project: add-movie, top5 and home.

#### Pages
Contains the components that describe the pages: login, register, list and pokedex

#### Shared folder
 `Shared`folder contains several utilities used inside of the module tgPI for the Top5 section. The other service is called `LocalMovies` that handles the addition and deletion of the movies between different sections. This serves as a local store of the modified information of the movies.

A `mock` folder was created under `shared` that has some test information used on the unit tests on different sections.

A `pipe` was created for giving format to the dates on different sections.

A `types`  folder was defined and contains the definition of types used on the application.

## Running the project

1. Run `npm install`
2. Run `ng serve` for a dev server. 
3. Navigate to `http://localhost:4200/` to view the application running

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Production setup

For the scope of the project, no configuration has been written for production other than the default Angular CLI configuration.